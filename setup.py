#coding:utf-8
import subprocess
from setuptools import find_packages, setup


EXCLUDE_FROM_PACKAGES = [
    "scripts",
    "pricespy_monitor.local",
]


def get_version():
    """
    """
    try:
        v = subprocess.check_output(["git", "describe"]).decode().strip().replace("v", "")
    except Exception:
        return "0.1"
    else:
        return v


setup(
    name="pricespy_monitor",
    version=get_version(),
    description="Price Spy",
    packages=find_packages(exclude=EXCLUDE_FROM_PACKAGES),
    package_data={
        "pricespy_monitor.configs": ["*.ini", "uwsgi/*", "locale/**"],
    },
    scripts=["pricespy_monitor/servers/server.sh"],
    entry_points={
        "console_scripts": [
            "pricespy-manage=pricespy_monitor.scripts.web_manage:main",
            "pricespy-init=pricespy_monitor.scripts.web_init:main",
            "pricespy-bot=pricespy_monitor.bot.bot:main",
        ],
    },
    install_requires=[
        "python-telegram-bot",
        "pysocks",
        "pyzbar",
        "opencv-python",
        "django",
        "mysqlclient",
        "uwsgi",
        "Pillow",
    ],
    include_package_data=True,
    zip_safe=False
)
