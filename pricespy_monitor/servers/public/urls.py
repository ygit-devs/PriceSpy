from django.conf import settings
from django.contrib import admin
from django.urls import path

from pricespy.views import ReportView


urlpatterns = [
    path("report/", ReportView.as_view()),
    path("", admin.site.urls)
]


if settings.DEBUG:
    from django.conf.urls.static import static

    urlpatterns +=  static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
      + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
