#!/usr/bin/env python
import argparse
import os
import shutil
import sys


BASE_DIR = os.path.join(os.path.dirname(__file__), "..")
rel = lambda x:os.path.normpath(os.path.join(BASE_DIR, x))


def touch(fname):
    """
    analog system touch command
    """
    if os.path.exists(fname):
        os.utime(fname, None)
    else:
        open(fname, 'a').close()


def mkdir(*args, **kwargs):
    """
    Wrapper for mkdir
    """
    mode = kwargs.get("mode", 0o755)
    for path in args:
        if not os.path.exists(path):
            os.mkdir(path, mode)
            os.chmod(path, mode)


class RunScript(object):
    """
    Run script actions
    """
    def __init__(self):
        """
        """
        pass

    def initialize(self):
        """
        Initialize base project FS structure
        """
        # create project work folders
        mkdir("local", "local/uwsgi", "static", "logs", ".pid")

        # touch local settings file
        touch("local/__init__.py")
        touch("local/settingslocal.py")

        # create uwsgi configs with full path of project
        self._copy_uwsgi_ini_files()

        print("Completed")

    def _copy_uwsgi_ini_files(self):
        """
        Copy uwsgi app configs with changed paths
        """
        shutil.copy2(rel("configs/emperor.ini"), "local/emperor.ini")

        for filename in os.listdir(rel("configs/uwsgi")):
            content = self._replace_ini_path("configs/uwsgi/%s" % filename)

            with open("local/uwsgi/%s" % filename, "w") as f:
                f.write(content)

    def _replace_ini_path(self, filename):
        """
        Replace paths in uwsgi ini config files
        """
        with open(rel(filename), "r") as f:
            content = f.read()
            content = content.replace("base=%d../..", "base=%s" % rel("."))

        return content


def parse_args(args=None):
    """
    Parse console arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--initialize", action="store_true",
                        help="Initialize project structure")
    # parser.add_argument("-u", "--uwsgi", action="store_true",
    #                     help="Start uwsgi daemons")
    # parser.add_argument("-p", "--processes", nargs='+', type=str, default=[], help="Processes for apply action")
    # parser.add_argument("action", type=str, help="Action")
    return parser, parser.parse_args(args)


def main(args=None):
    """
    Main running function
    """
    if args is None:
        args = sys.argv

    # parser cli parguments
    parser, ns = parse_args(args[1:])

    actor = RunScript()
    if ns.initialize:
        actor.initialize()
    else:
        parser.print_help()

    ### parse config
    # config = get_config_module(cli_args.config)

    ### create new object of runner class
    # runner_class = config.pop("runner_class")
    # runner_class(config).run(cli_args.action, *cli_args.processes)


if __name__ == "__main__":
    main(sys.argv)
