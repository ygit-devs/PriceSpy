#!/usr/bin/env python
import os
import sys


def main():
    # get script absolute path
    pwd = os.path.normpath(os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))

    # pop application name from console arguments
    if len(sys.argv) < 2:
        print("Example of usage: %s <app_name>" % sys.argv[0])
        sys.exit(1)

    app_name = sys.argv.pop(1)
    if not os.path.exists("%s/servers/%s" % (pwd, app_name)):
        print("No application directory '%s'" % app_name)
        sys.exit(1)

    # update PYTHONPATH environ
    path_str = "{2}:{0}/servers/{1}:{0}:{0}/modules:".format(pwd, app_name, os.getcwd())
    os.environ.setdefault("PYTHONPATH", path_str)

    # call original manage.py script   
    os.system("%s %s/configs/manage.py %s" % (sys.executable, pwd, " ".join(sys.argv[1:])))


if __name__ == "__main__":
    main()
