from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import User, TradePoint, SpyRecord


class UserAdmin(admin.ModelAdmin):
    """
    """
    list_display = ("nickname", "first_name", "last_name", "is_enabled", "created_at")
    search_fields = ("nickname", "first_name", "last_name")
    list_filter = ("created_at", "is_enabled")


class TradePointAdmin(admin.ModelAdmin):
    """
    """
    list_display = ("name", "address", "lat", "lon")
    list_filter = ("created_at", "is_enabled")
    search_fields = ("name", "address")


class SpyRecordAdmin(admin.ModelAdmin):
    """
    """

    list_display = ("barcode", "barcode_image_small", "price", "user", "trade_point", "created_at")
    list_filter = ("trade_point", "created_at")
    search_fields = ("barcode", )
    fields = ("user", "trade_point", "barcode", "price", 'barcode_image_full', )
    readonly_fields = ('barcode_image_full', "user", "trade_point")

    def barcode_image_small(self, obj):
        """
        """
        return mark_safe("""<img src="{}" height=80>""".format(obj.image.image.url if obj.image else ""))
    barcode_image_small.short_description = "Штрихкод"

    def barcode_image_full(self, obj):
        """
        """
        return mark_safe("""<img src="{}">""".format(obj.image.image.url if obj.image else ""))
    barcode_image_full.short_description = "Штрихкод"


admin.site.register(User, UserAdmin)
admin.site.register(TradePoint, TradePointAdmin)
admin.site.register(SpyRecord, SpyRecordAdmin)
