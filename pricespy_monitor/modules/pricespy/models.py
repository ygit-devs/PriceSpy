from django.db import models
from django.db.models.deletion import CASCADE  # , SET_NULL
# from django.contrib.gis.db.models import PointField


class User(models.Model):
    """
    User in bot
    """
    telegram_id = models.CharField("ID", max_length=200, unique=True)
    nickname = models.CharField("Nickname", max_length=200)
    first_name = models.CharField("First name", max_length=200, blank=True)
    last_name = models.CharField("Last name", max_length=200, blank=True)
    is_enabled = models.BooleanField("Enabled", help_text="Is user enabled in system", default=False)
    created_at = models.DateTimeField("Create date", auto_now_add=True)

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"

    def __str__(self):
        """
        Object string representation
        """
        return self.nickname


class TradePoint(models.Model):
    """
    Model for trade point (shop) instance
    """
    name = models.CharField("Name", max_length=200)
    address = models.CharField("Address", max_length=200)
    # coords = PointField("Coordinates", max_length=200)
    lat = models.FloatField("Latitude")
    lon = models.FloatField("Longitude")
    # coords = models.TextField("Coordinates", help_text="separator is whitespace", max_length=200)
    created_at = models.DateTimeField("Create date", auto_now_add=True)
    is_enabled = models.BooleanField("Enabled", help_text="Is trade point enabled in system", default=True)

    class Meta:
        verbose_name = 'Trade point'
        verbose_name_plural = 'Trade points'

    def __str__(self):
        """
        Object string representation
        """
        return self.name


class BarCode(models.Model):
    """
    Model for save barcode images
    """
    user = models.ForeignKey(User, on_delete=CASCADE)
    trade_point = models.ForeignKey(TradePoint, verbose_name="Shop", on_delete=CASCADE)
    origin_url = models.CharField("Original image url", max_length=200)
    image = models.ImageField(upload_to="barcodes/")
    created_at = models.DateTimeField("Create date", auto_now_add=True)

    class Meta:
        verbose_name = "Bar code"
        verbose_name_plural = "Bar codes"

    def __str__(self):
        """
        Object string representation
        """
        return self.origin_url


class SpyRecord(models.Model):
    """
    Record of received price
    """
    user = models.ForeignKey(User, on_delete=CASCADE)
    trade_point = models.ForeignKey(TradePoint, verbose_name="Shop", on_delete=CASCADE)
    image = models.ForeignKey(BarCode, verbose_name="Bar code image", on_delete=CASCADE,
                              null=True, default=None)
    barcode_link = models.CharField("Barcode link", max_length=255)
    barcode = models.CharField("Barcode", max_length=100)
    price = models.CharField("Price", max_length=50)
    created_at = models.DateTimeField("Create date", auto_now_add=True)

    class Meta:
        verbose_name = 'SpyRecord'
        verbose_name_plural = 'SpyRecords'

    def __str__(self):
        """
        """
        return "{} ({})".format(self.barcode, self.trade_point_id)
