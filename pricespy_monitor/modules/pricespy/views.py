import datetime

from django.views.generic import View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse

from .models import SpyRecord


def parse_date(value):
    """
    """
    if not value:
        return None

    try:
        return datetime.datetime.strptime(value, "%d-%m-%Y")
    except:
        return None


class ReportView(LoginRequiredMixin, View):
    """
    XML report view
    """
    def get(self, *args, **kwargs):
        """
        """
        data = self.request.GET
        params = {}
        for key in ["barcode", "user_id", "shop_id"]:
            if key in data:
                params[key] = data[key]
                if "_id" in key:
                    try:
                        params[key] = int(params[key])
                    except:
                        pass

        if "price_min" in data:
            params["price__gte"] = data["price_min"]

        if "price_max" in data:
            params["price__lte"] = data["price_max"]

        if "date_from" in data:
            date_from = parse_date(data["date_from"])
            if date_from:
                params["created_at__gte"] = date_from

        if "date_to" in data:
            date_to = parse_date(data["date_to"])
            if date_to:
                params["created_at__lte"] = date_to

        records = []
        for item in SpyRecord.objects.select_related().order_by("-id").filter(**params):
            records.append("""
<record>
    <barcode>{}</barcode>
    <price>{}</price>
    <user>{}</user>
    <shop>{}</shop>
    <date>{}</date>
</record>""".format(item.barcode, item.price, item.user, item.trade_point, item.created_at.strftime("%d.%m.%Y %H:%I:%S")))

        return HttpResponse("<root>{}</root>".format("".join(records)), content_type="text/xml")
