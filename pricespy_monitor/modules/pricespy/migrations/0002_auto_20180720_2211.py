# Generated by Django 2.0.7 on 2018-07-20 19:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pricespy', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='telegram_id',
            field=models.CharField(max_length=200, unique=True, verbose_name='ID'),
        ),
    ]
