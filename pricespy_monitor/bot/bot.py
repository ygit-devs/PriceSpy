#!/usr/bin/env python
import hashlib
import logging
import os
import sys
from uuid import uuid4

import django
from django.core.files.base import ContentFile
from django.db import close_old_connections
from telegram import InlineKeyboardMarkup, InlineKeyboardButton, ReplyKeyboardMarkup, KeyboardButton
from telegram.ext import Updater, CommandHandler, MessageHandler, CallbackQueryHandler, Filters

sys.path.insert(0, ".")
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../modules")))
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../local")))

from pricespy_monitor.configs.settings import BOT_TOKEN, BOT_REQUEST_KWARGS, \
    BOT_SHOPS_COUNT, BOT_SHOP_DISTANCE
from pricespy_monitor.bot.tools import parse_barcode, coords_distance, TextFilter

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "pricespy_monitor.configs.settings")
django.setup()

from pricespy.models import User, TradePoint, SpyRecord, BarCode

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("bot")


def get_file_content(bot, url):
    """
    """
    try:
        result = bot._request._request_wrapper('GET', url)
    except Exception:
        return None
    else:
        return result


def get_md5(text):
    """
    MD5 function short name
    """
    return hashlib.md5(text.encode("utf-8")).hexdigest()


def save_barcode(user, url, content):
    """
    Save barcode image to internal storage
    """
    instance = BarCode(user_id=user.user_id, trade_point_id=user.shop_id,
                       origin_url=url)

    name = get_md5(str(uuid4()))
    parts = (name[:2], name[2:4], name[4:6], name)
    instance.image.save(
        "{}/{}/{}/{}".format(*parts),
        ContentFile(content)
    )
    return instance


class UserState:
    """
    Chat user session state
    """
    user_id = None
    is_enabled = False
    shop_id = None
    image_id = None
    barcode_link = None
    barcode = None
    price = None
    lat = None
    lon = None

    def reset(self):
        """
        Reset user state
        """
        self.shop_id = None
        self.image_id = None
        self.barcode_link = None
        self.barcode = None
        self.price = None
        self.lat = None
        self.lon = None


def get_user(update):
    """
    Get users session object
    """
    chat = update.message.chat
    default_data = {"first_name": chat.first_name, "nickname": chat.username}

    close_old_connections()
    user, created = User.objects.get_or_create(telegram_id=chat.id, defaults=default_data)
    if not user.is_enabled:
        update.reply_text("Аккаунт не активирован. Дождитесь активации администратором.")
        logger.warning("User {} disabled".format(user))
        return None

    if chat.id not in users:
        users[chat.id] = UserState()
    users[chat.id].user_id = user.id

    return users[chat.id]


users = {}


def start(bot, update):
    """
    Bot start command
    """
    user = get_user(update)
    if not user:
        return

    keyboard = ReplyKeyboardMarkup([
            [KeyboardButton("Начать сбор")],
        ],
        resize_keyboard=True,
    )

    try:
        bot.send_message(
            update.message.chat_id,
            'Приветствуем тебя, {}!'.format(update.message.from_user.first_name),
            reply_markup=keyboard
        )
    except Exception as e:
        logger.exception(e)


def start_work(bot, update):
    """
    Start work with bot
    """
    user = get_user(update)
    if not user:
        return

    keyboard = ReplyKeyboardMarkup([
            [
                KeyboardButton("Магазины рядом со мной", request_location=True),
                KeyboardButton("Показать все магазины")
            ],
        ],
        resize_keyboard=True,
    )

    try:
        bot.send_message(
            update.message.chat_id,
            'Выберите дальнейшее действие',
            reply_markup=keyboard
        )
    except Exception as e:
        logger.exception(e)


def end_work(bot, update):
    """
    End work with bot
    """
    user = get_user(update)
    if not user:
        return

    keyboard = ReplyKeyboardMarkup([
            [KeyboardButton("Начать сбор")],
        ],
        resize_keyboard=True,
    )

    try:
        user.reset()
        bot.send_message(
            update.message.chat_id,
            'Работа завершена',
            reply_markup=keyboard
        )
    except Exception as e:
        logger.exception(e)


def callback_handler(bot, update):
    """
    Handler for inline keyboard buttons
    """
    try:
        if not update.callback_query.data:
            return

        user = get_user(update.callback_query)
        if not user:
            return

        if update.callback_query.data == "start_work":
            start_work(bot, update)

        elif update.callback_query.data.startswith("shop_"):
            # select shop
            shop_id = update.callback_query.data.replace("shop_", "")
            try:
                shop = TradePoint.objects.get(is_enabled=True, id=shop_id)
            except Exception:
                pass
            else:

                keyboard = ReplyKeyboardMarkup([
                        [
                            KeyboardButton("Магазин: {} {}".format(shop.name, shop.address)),
                            KeyboardButton("Завершить работу".format(shop.name, shop.address)),
                        ],
                    ],
                    resize_keyboard=True,
                    # one_time_keyboard=True,
                )

                user.shop_id = shop.id
                bot.send_message(
                    update.callback_query.message.chat.id,
                    "Выбран магазин: {} {}\nЗагрузите штрих код или выберите другой магазин".format(shop.name, shop.address),
                    reply_markup=keyboard
                )

    except Exception as e:
        logger.exception(e)


def location_handler(bot, update):
    """
    Request geo location handler
    """
    user = get_user(update)
    if not user:
        return

    user.lat = update.message.location.latitude
    user.lon = update.message.location.longitude

    shops_list(bot, update, True)


def create_shops_menu_keyboard(items_list):
    """
    Create inline keyboard with shops list
    """
    shops_list = []
    shops_row = []
    for item in items_list:
        if len(shops_row) == 2:
            shops_list.append(shops_row)
            shops_row = []

        shops_row.append(InlineKeyboardButton("{}".format(item.name), callback_data='shop_{}'.format(item.id)))

    if len(shops_row):
        shops_list.append(shops_row)

    return InlineKeyboardMarkup(shops_list)


def shops_list(bot, update, nearest=False):
    """
    Get list of shops
    """
    try:

        user = get_user(update)
        if not user:
            return

        items_list = []
        if nearest and user.lat and user.lon:
            # filter by distance
            user_point = (user.lat, user.lon)
            items_by_dist = []
            for item in TradePoint.objects.filter(is_enabled=True):
                dist = coords_distance(user_point, (item.lat, item.lon))
                items_by_dist.append((dist, item))

            for dist, item in sorted(items_by_dist, key=lambda x: x[0]):
                if len(items_list) >= BOT_SHOPS_COUNT:
                    break
                # if dist > BOT_SHOP_DISTANCE:
                #     break

                items_list.append(item)
                # if len(items_list):
                #     break

                # if len(items_list) < BOT_SHOPS_COUNT:
                #     items_list.append(item)
                # else:
                #     break
        else:
            items_list = TradePoint.objects.filter(is_enabled=True)

        if not len(items_list) and nearest:
            bot.send_message(
                update.message.chat_id,
                'Магазином в радиусе 500м не найдено, выберите из полного списка'
            )
            return

        keyboard = create_shops_menu_keyboard(items_list)

        bot.send_message(
            update.message.chat_id,
            'Пожалуйста, выберите магазин из списка ниже',
            reply_markup=keyboard
        )
    except Exception as e:
        logger.exception(e)


def photo_handler(bot, update):
    """
    Receive barcode image, decode it and save into user session
    """
    try:
        max_width = 0
        file_id = None
        for photo in update.message.photo:
            if photo.width > max_width:
                max_width = photo.width
                file_id = photo.file_id

        if update.message.document is not None:
            file_id = update.message.document.file_id

        if not file_id:
            return

        user = get_user(update)
        if not user:
            return

        input_file = bot.getFile(file_id)
        content = get_file_content(bot, input_file.file_path)
        if len(content):
            # save barcode image
            data = parse_barcode(content)
            if len(data) == 0:
                logger.error("Parse barcode error: %s", input_file.file_path)
                update.message.reply_text('Не удалось распознать штрих-код. Загрузите другое изображение.')
                return

            elif len(data) > 1:
                logger.error("More than 1 barcode on image: %s", input_file.file_path)
                update.message.reply_text('На одном изображении может быть только один код.')
                return

            else:
                image = save_barcode(user, input_file.file_path, content)
                user.image_id = image.id
                user.barcode = data[0]
                user.barcode_link = input_file.file_path

        else:
            logger.error("Upload image error: %s", input_file.file_path)
            update.message.reply_text('Произошла ошибка загрузки изображения. Пожалуйста, повторите попытку позже.')
            return

        update.message.reply_text('Распознан код: {}. Введите цену товара.'.format(user.barcode))
    except Exception as e:
        logger.exception(e)


def price_handler(bot, update):
    """
    Receive product price and save data to DB
    """
    user = get_user(update)
    if not user:
        return

    if update.message.text.startswith("Магазин: "):
        shops_list(bot, update)
        return

    if not user.shop_id:
        update.message.reply_text("Сообщение не обработано. Необходимо выбрать магазин.")
        return

    if not user.barcode:
        update.message.reply_text("Сперва необходимо загрузить штрихкод")
        return

    user.price = str(update.message.text)
    info = {"code": user.barcode, "price": user.price}

    # save to DB
    try:
        SpyRecord(user_id=user.user_id, trade_point_id=user.shop_id,
                  barcode_link=user.barcode_link, barcode=user.barcode,
                  price=user.price, image_id=user.image_id).save()

        user.barcode = None
        user.price = None
        update.message.reply_text(
            ('Данные сохранены. Штрихкод: {}, цена: {}\n\n'
             'Загрузите фотографию штрих-кода').format(info["code"], info["price"])
        )
    except Exception as e:
        logger.exception(e)


def main():
    """
    Run process
    """
    updater = Updater(BOT_TOKEN, request_kwargs=BOT_REQUEST_KWARGS)

    updater.dispatcher.add_handler(CommandHandler('start', start))
    updater.dispatcher.add_handler(MessageHandler(TextFilter('Начать сбор'), start_work))
    updater.dispatcher.add_handler(MessageHandler(TextFilter('Показать все магазины'), shops_list))
    updater.dispatcher.add_handler(MessageHandler(TextFilter('Завершить работу'), end_work))
    updater.dispatcher.add_handler(MessageHandler(Filters.location, location_handler))
    updater.dispatcher.add_handler(MessageHandler(Filters.photo | Filters.document, photo_handler))
    updater.dispatcher.add_handler(MessageHandler(Filters.text, price_handler))
    updater.dispatcher.add_handler(CallbackQueryHandler(callback_handler))

    # updater.start_webhook(listen='127.0.0.1', port=5000, url_path='TOKEN1')
    # updater.bot.set_webhook(webhook_url='https://example.com/TOKEN1',
    #                         certificate=open('cert.pem', 'rb'))
    # updater.start_webhook(listen='0.0.0.0',
    #                       port=8443,
    #                       url_path='TOKEN',
    #                       key='private.key',
    #                       cert='cert.pem',
    #                       webhook_url='https://example.com:8443/TOKEN')
    # updater.bot.set_webhook("https://<appname>.herokuapp.com/" + TOKEN)
    updater.start_polling()
    logger.info("START")
    updater.idle()


if __name__ == "__main__":
    main()
