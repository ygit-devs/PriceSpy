import tempfile
from math import sin, cos, sqrt, atan2, radians

import pyzbar.pyzbar as pyzbar
import cv2
from telegram.ext import BaseFilter


def decode(im):
    # Find barcodes and QR codes
    return pyzbar.decode(im)


def parse_barcode(data):
    """
    Get list of parsed barcodes
    """
    # save image to temporary file
    tmp = tempfile.NamedTemporaryFile()
    tmp.write(data)

    im = cv2.imread(tmp.name)

    decoded_objects = decode(im)

    result = []
    for item in decoded_objects:
        if item.type == "EAN13":
            result.append(item.data.decode())
        else:
            print("Unexpected data: {} {}", item.type, item.data)

    return result


def coords_distance(point1, point2):
    """
    Calculate distance between 2 coordinates points
    """
    # approximate radius of earth in km
    R = 6373.0

    lat1 = radians(point1[0])
    lon1 = radians(point1[1])
    lat2 = radians(point2[0])
    lon2 = radians(point2[1])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    return R * c
    # print("Result:", distance)
    # print("Should be:", 278.546, "km")


class TextFilter(BaseFilter):
    def __init__(self, pattern):
        self.pattern = pattern
        self.name = 'TextFilter({})'.format(self.pattern)

    def filter(self, message):
        return bool(message.text and message.text == self.pattern)
